import CRFrameworkCoreLogic

internal enum TestGroup: NSString {
    case none, control, review, mail_feedback
}

internal final class RemoteConfigStorage {
    private let storageContainer: StorageContainer<Keys>
       
    internal init(storage: CRFrameworkCoreLogic.Storage = UserDefaults.standard) {
        self.storageContainer = storage.storageContainer(for: Keys.self)
    }
    
    internal func storeTextColor(textColor: String) {
        self.textColor = textColor
    }
    
    internal func storeViewBackgroundColor(viewBackgroundColor: String) {
        self.viewBackgroundColor = viewBackgroundColor
    }
    
    internal func storeABTestGroup(abTestGroup: TestGroup) {
        self.abTestGroup = abTestGroup
    }
}

// MARK: - Variables
extension RemoteConfigStorage {
    internal var viewBackgroundColor: String? {
        get { return storageContainer.string(for: .viewBackgroundColor) }
        set { storageContainer.set(newValue, key: .viewBackgroundColor) }
    }
    
    internal var textColor: String? {
        get { return storageContainer.string(for: .textColor) }
        set { storageContainer.set(newValue, key: .textColor) }
    }
    
    internal var abTestGroup: TestGroup {
        get {
            guard let value = storageContainer.string(for: .abTestGroup) else {
                return TestGroup.none
            }
            return TestGroup(rawValue: NSString(string: value)) ?? TestGroup.none
        }
        set { storageContainer.set(String(newValue.rawValue), for: .abTestGroup) }
    }
}

// MARK: - Storage Keys
extension RemoteConfigStorage {
    private enum Keys: String, UserDefaultsKey {
        case textColor
        case viewBackgroundColor
        case abTestGroup
        
        fileprivate var storageKey: String {
            return "RemoteConfigStorage." + rawValue
        }
    }
}
