import AppBootstrap

internal final class RemoteConfigUtils {
    private let remoteConfigStorage = RemoteConfigStorage()
    
    internal func setupRemoteConfigDefaultValues() {
        let labelTextColorKey = RemoteConfigKey.labelTextColor.rawValue
        let labelTextColorDefaultValue = RemoteConfigKeyDefaultValue.labelTextColor.rawValue
        
        let viewBackgroundColorKey = RemoteConfigKey.viewBackgroundColor.rawValue
        let viewBackgroundColorDefaultValue = RemoteConfigKeyDefaultValue.viewBackgroundColor.rawValue
        
        let feedbackABTestGroupKey = RemoteConfigKey.feedbackGroup.rawValue
        let feedbackABTestGroupDefaultValue = RemoteConfigKeyDefaultValue.feedbackGroup.rawValue

        if RemoteConfigHelper.shared.string(forKey: labelTextColorKey).isEmpty {
            RemoteConfigHelper.shared.setDefaults([labelTextColorKey: labelTextColorDefaultValue])
            remoteConfigStorage.storeTextColor(textColor: String(labelTextColorDefaultValue))
        }
        
        if RemoteConfigHelper.shared.string(forKey: viewBackgroundColorKey).isEmpty {
            RemoteConfigHelper.shared.setDefaults([viewBackgroundColorKey: viewBackgroundColorDefaultValue])
            remoteConfigStorage.storeViewBackgroundColor(viewBackgroundColor: String(viewBackgroundColorDefaultValue))
        }
        
        if RemoteConfigHelper.shared.string(forKey: feedbackABTestGroupKey).isEmpty {
            RemoteConfigHelper.shared.setDefaults([feedbackABTestGroupKey: feedbackABTestGroupDefaultValue])
            if let group = TestGroup(rawValue: feedbackABTestGroupDefaultValue) {
                remoteConfigStorage.storeABTestGroup(abTestGroup: group)
            } else {
                remoteConfigStorage.storeABTestGroup(abTestGroup: .none)
            }
        }
    }
}

// MARK: - Remote Config Key
extension RemoteConfigUtils {
    internal enum RemoteConfigKey: String {
        case labelTextColor = "remote_config_key_label_text_color"
        case viewBackgroundColor = "remote_config_key_view_background_color"
        case feedbackGroup = "feedback_group"
    }
    
    internal enum RemoteConfigKeyDefaultValue: NSString {
        case labelTextColor = "black"
        case viewBackgroundColor = "white"
        case feedbackGroup = "control"
    }
}
