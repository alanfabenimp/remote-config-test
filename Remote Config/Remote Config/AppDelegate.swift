import UIKit
import AppBootstrap
import FirebaseInstanceID
import CRFrameworkCoreUI

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    private let remoteConfigUtils = RemoteConfigUtils()
    private let remoteConfigViewController = RemoteConfigViewController()
    
    internal var rootViewController: RemoteConfigViewController?
    
    internal static var instance: AppDelegate {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            fatalError("Delegate not available.")
        }
        
        return appDelegate
    }
    
    internal let context: AppContext = {        
        let config = AppContextConfig(impalaAppIdentifier: .flightTrackerFree)
        config.enableInAppMarketing = false
        config.enableAnalytics = true
        config.enablePopupConductor = false
        config.enableAdvertisementManager = false
             
        return AppContext(config: config)
    }()

    internal func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        _ = context.initialize(consentDelegate: self, launchOptions: launchOptions)
        
        InstanceID.instanceID().instanceID { result, _ in
            print("Remote instance id: \(result?.token ?? "")")
        }
        
        setupWindow()
        
        remoteConfigUtils.setupRemoteConfigDefaultValues()
        fetchRemoteConfigValues()

        return true
    }
    
    internal func applicationWillEnterForeground(_ application: UIApplication) {
        fetchRemoteConfigValues()
    }
    
    private func fetchRemoteConfigValues() {
        RemoteConfigHelper.shared.fetchCloudValues()
        RemoteConfigHelper.shared.delegates.add(self)
    }
    
    private func setupWindow() {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = remoteConfigViewController
        window?.makeKeyAndVisible()
        
        rootViewController = remoteConfigViewController
    }
}

// MARK: - Remote Config Helper Delegate
extension AppDelegate: RemoteConfigHelperDelegate {
    internal func remoteConfigValuesFetchedAndActivated() {
        guard let textColor = RemoteConfigHelper.shared.string(forKey: RemoteConfigUtils.RemoteConfigKey.labelTextColor.rawValue), textColor.isNotEmpty else {
            assertionFailure("Text color should not be empty")
            return
        }

        guard let viewBackgroundColor = RemoteConfigHelper.shared.string(forKey: RemoteConfigUtils.RemoteConfigKey.viewBackgroundColor.rawValue), viewBackgroundColor.isNotEmpty else {
            assertionFailure("View background color should not be empty")
            return
        }
        
        guard let feedbackABTestGroup = RemoteConfigHelper.shared.string(forKey: RemoteConfigUtils.RemoteConfigKey.feedbackGroup.rawValue), feedbackABTestGroup.isNotEmpty else {
            assertionFailure("AB test group should not be empty")
            return
        }
        
        let remoteConfigStorage = RemoteConfigStorage()

        remoteConfigStorage.storeTextColor(textColor: textColor)
        remoteConfigStorage.storeViewBackgroundColor(viewBackgroundColor: viewBackgroundColor)
        
        if let group = TestGroup(rawValue: NSString(string: feedbackABTestGroup)) {
            remoteConfigStorage.storeABTestGroup(abTestGroup: group)
        } else {
            remoteConfigStorage.storeABTestGroup(abTestGroup: .none)
        }
        
        DispatchTools.onMain { [weak self] in
            self?.rootViewController?.loadValuesFromStorage()
        }
    }
}

// MARK: - App Context Consent Delegate
extension AppDelegate: AppContextConsentDelegate {
    internal func appContext(_ context: AppContext, queryRequiredFor consentDetails: ConsentDetails) {}
    internal func appContext(_ context: AppContext, consentUpdated consentDetails: ConsentDetails) {}
}
