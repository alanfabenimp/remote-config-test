import CRFrameworkCoreUI

extension RemoteConfigViewController {
    internal class View: UIView {
        private let remoteConfigNameLabel = UILabel()
        private let remoteConfigStorage = RemoteConfigStorage()
        
        internal override init(frame: CGRect) {
            super.init(frame: frame)
            
            configureViews()

            loadValuesFromStorage()
        }
        
        internal required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        internal func loadValuesFromStorage() {            
            guard let textColor = remoteConfigStorage.textColor, let viewBackgroundColor = remoteConfigStorage.viewBackgroundColor else {
                return
            }
                        
            remoteConfigNameLabel.textColor = UIColor(hexColor: textColor)
            backgroundColor = UIColor(hexColor: viewBackgroundColor)
        }
    }
}
 
// MARK: - Configure Views
extension RemoteConfigViewController.View {
    private func configureViews() {
        backgroundColor = .white
    
        [remoteConfigNameLabel]
            .disableTranslateAutoresizingMask()
            .add(to: self)
    
        configureRemoteConfigNameLabel()
    }

    private func configureRemoteConfigNameLabel() {
        remoteConfigNameLabel.text = "Impala Studios"
        remoteConfigNameLabel.font = .systemFont(ofSize: 16, weight: .bold)
        remoteConfigNameLabel.textColor = .black
        remoteConfigNameLabel.textAlignment = .center
    
        remoteConfigNameLabel.pinTopToSuperview(padding: 32, layoutArea: .safeArea)
        remoteConfigNameLabel.pinCenterHorizontalToSuperview()
    }
}
