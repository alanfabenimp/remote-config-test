import CRFrameworkCoreUI
import AppBootstrap
import StoreKit

internal final class RemoteConfigViewController: UIViewController {
    private lazy var rootView = View(frame: .keyWindowBounds)
    private let remoteConfigStorage = RemoteConfigStorage()
    
    internal override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)

        view = rootView
    }
    
    private func presentReview() {
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
        }
    }
    
    private func presentFeedbackEmail() {
        let appInfo = AppDelegate.instance.context.info

        let emailController = SupportEmailController(appCode: appInfo.impalaAppIdentifier.appCode,
                                                     appName: appInfo.impalaAppIdentifier.description,
                                                     appVersion: appInfo.bundleVersion,
                                                     appBuildNumber: appInfo.bundleBuild)
        
        emailController.showEmailComposerWithViewController(self)
    }
    
    internal func loadValuesFromStorage() {
        rootView.loadValuesFromStorage()
        
        switch remoteConfigStorage.abTestGroup {
        case .review:
            presentReview()
        case .mail_feedback:
            presentFeedbackEmail()
        case .none, .control:
            print("nothing to do, the behaviour remains the same")
        }
    }
}
